# Simple API to search for cities and countries

This very easy to use API offers the functionality to search for cities with a specific name. 

``` 
/api/city/Berlin
/api/city/Berlin/2

/api/citiesofacountry/Germany
/api/citiesofacountry/Germany/10

/api/allcities/10
```

All the search responses are sorted by the cities' population (biggest first).

Result is a standard JSON containing the cities' name, its population, its country/state and its longitude/latitude.

```json
{
    "cities": [{
        "name": "Berlin",
        "state": "Berlin",
        "country": "Germany",
        "longitude": "52.520008",
        "latitude": "13.0404954",
        "population": "3575000"
    }]
}
```

# Prerequisites
Running on python 2.7 with the following libraries that need to be installed.

- pip install iso3166
- pip install flask_restful

# Usage

```bash
python2.7 app.py
```

If you want to run this on a remote server, please consider reading the appropriate documentation. Setting hostname and port can be done within `app.py`.

# Thank you
This API is running on geo data provided by geonames.org (https://www.geonames.org/). Thank you for publishing that.



I hope someone can use this API. Feel free to use it as you want.
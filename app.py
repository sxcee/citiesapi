from flask import Flask
from flask_restful import Api, Resource, reqparse
from iso3166 import countries
from operator import itemgetter

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

if __name__ == "__main__":
  app = Flask(__name__)
  api = Api(app)

  # 0 geonameid         : integer id of record in geonames database
  # 1 name              : name of geographical point (utf8) varchar(200)
  # 2 asciiname         : name of geographical point in plain ascii characters, varchar(200)
  # alternatenames    : alternatenames, comma separated, ascii names automatically transliterated, convenience attribute from alternatename table, varchar(10000)
  # 4 latitude          : latitude in decimal degrees (wgs84)
  # 5 longitude         : longitude in decimal degrees (wgs84)
  # feature class     : see http://www.geonames.org/export/codes.html, char(1)
  # feature code      : see http://www.geonames.org/export/codes.html, varchar(10)
  # 8 country code      : ISO-3166 2-letter country code, 2 characters
  # cc2               : alternate country codes, comma separated, ISO-3166 2-letter country code, 200 characters
  # admin1 code       : fipscode (subject to change to iso code), see exceptions below, see file admin1Codes.txt for display names of this code; varchar(20)
  # admin2 code       : code for the second administrative division, a county in the US, see file admin2Codes.txt; varchar(80) 
  # admin3 code       : code for third level administrative division, varchar(20)
  # admin4 code       : code for fourth level administrative division, varchar(20)
  # population        : bigint (8 byte int) 
  # elevation         : in meters, integer
  # dem               : digital elevation model, srtm3 or gtopo30, average elevation of 3''x3'' (ca 90mx90m) or 30''x30'' (ca 900mx900m) area in meters, integer. srtm processed by cgiar/ciat.
  # timezone          : the iana timezone id (see file timeZone.txt) varchar(40)
  # modification date : date of last modification in yyyy-MM-dd format

  cityList = []

  cityFile = open("citieswithpop.csv", "r")
  cities = cityFile.readlines()

  for city in cities[1:]:
    city = city.replace("\n","")
    tmpCity = city.split("|")
    try:
      cityList.append({
        "name": tmpCity[2],
        "state": tmpCity[3],
        "country": countries.get(tmpCity[5]).name,
        "longitude": tmpCity[1],
        "latitude": tmpCity[0],
        "population": int(tmpCity[6])
      })
    except KeyError as err:
      print city
      print err

  def getCountryFromCode(value):
    print value

  cityFile.close()


  class City(Resource):
    def get(self, name, quantity=-1):
      res = []
      for city in cityList:
        if(city["name"].lower().startswith(name.lower())):
          res.append(city)

      newlist = sorted(res, key=itemgetter("population"), reverse=True) 
      moreresults = False
      if len(newlist) > quantity:
        moreresults = True
      
      return {"cities": newlist[:quantity] if moreresults && quantity != -1 else newlist}, 200


  class BiggestCities(Resource): 
    def get(self, quantity):
      newlist = sorted(cityList, key=itemgetter("population"), reverse=True)

      moreresults = False
      if len(newlist) > quantity:
        moreresults = True 
      
      return {"cities": newlist[:quantity] if moreresults else newlist}, 200

  class CitiesOfCountry(Resource):
    def get(self, country, quantity=-1):
      res = []
      for city in cityList:
        if(city["country"].lower().startswith(country.lower())):
          res.append(city)

      newlist = sorted(res, key=itemgetter("population"), reverse=True)
      moreresults = False
      if len(newlist) > quantity:
        moreresults = True
      
      return {"cities": newlist[:quantity] if moreresults && quantity != -1 else newlist}, 200
  
  api.add_resource(City, "/city/<string:name>")     
  api.add_resource(City, "/city/<string:name>/<int:quantity>")
  api.add_resource(BiggestCities, "/biggestcities/<int:quantity>")
  api.add_resource(CitiesOfCountry, "/citiesofacountry/<string:country>")
  api.add_resource(CitiesOfCountry, "/citiesofacountry/<string:country>/<int:quantity>")

  # Change Host and or Port according to your environment. For Production set debug=False.
  app.run(
    host="0.0.0.0", 
    # port=1234, 
    debug=True)
